$('#search').keyup(function () {
    //get data from json file
    //var urlForJson = "data.json";


    //get data from Restful web Service in development environment
    var urlForJson = "/api/talent";      // Uses ProductStore HTTP Web API
    //"https://localhost:44326/api/talents";      //ProductStore Secure HTTPS Web API 



    //get data from Restful web Service in production environment
    //var urlForJson= "http://csc123.azurewebsites.net/api/talents";

    //Url for the Cloud image hosting
    var urlForCloudImage = "https://talentimages.s3-ap-southeast-1.amazonaws.com/";
    //"http://res.cloudinary.com/duraqdesh/image/upload/v1560688076/images/";  // jshafeerdeen.work Cloudinary Account PW: Sh@**rd**n***

    var searchField = $('#search').val();
    var myExp = new RegExp(searchField, "i");
    $.getJSON(urlForJson, function (data) {
        var output = '<ul class="searchresults">';
        $.each(data, function (key, val) {
            console.log(data);
            if ((val.Name.search(myExp) != -1) ||
			(val.Bio.search(myExp) != -1)) {
                output += '<li>';
                output += '<h2>' + val.Name + '</h2>';
                //get the absolute path for local image
                //output += '<img src="images/'+ val.ShortName +'_tn.jpg" alt="'+ val.Name +'" />';
                
                //get the image from cloud hosting
                output += ' <img src=' + urlForCloudImage + val.ShortName + "_tn.jpg alt=" + val.Name + '" />';
                output += '<p>' + val.Bio + '</p>';
                output += '<button class="btn btn-info" onclick=editTalents(' + val.Id + ')>Edit</button><button class="btn btn-danger" onclick=deleteTalents(' + val.Id + ')>Delete</button>'
                output += '</li>';
            }
        });
        output += '</ul>';
        $('#update').html(output);
    }); //get JSON
});

function deleteTalents(id) { 

    confirmDelete("Confirm Delete Talent", (ans) => {
        if (ans) {
            $.ajax({
                url: "api/talent/" + id,
                type: 'DELETE',
                success: function (result) {
                    alert("Deleted Talent Successfully");
                    document.getElementById("search").value = "";
                }
            });
        } else {
            
        }
    });
}

function editTalents(id) {

    $.ajax({
        url: "/api/talent/" + id,
        type: 'GET',
        contentType:
            "application/json;charset=utf-8",
        success: function (objs) {
            document.getElementById("editID").value = id;
            document.getElementById("editName").value = objs.Name;
            document.getElementById("editShortName").value = objs.ShortName;
            document.getElementById("editReknown").value = objs.Reknown;
            document.getElementById("editBio").value = objs.Bio;}
    });

    confirmEdit("Edit Talent", (ans) => {
        if (ans) {
            var talents = new Object();
            talents.Id = document.getElementById("editID").value;
            talents.Name = document.getElementById("editName").value;
            talents.ShortName = document.getElementById("editShortName").value;
            talents.Reknown = document.getElementById("editReknown").value;
            talents.Bio = document.getElementById("editBio").value;

            $.ajax({
                url: "api/talent",
                type: 'PUT',
                dataType: 'json',
                data: talents,
                success: function (result) {
                    alert("Edited Talent Successfully");
                    document.getElementById("search").value = "";
                }
            });

        } else {
            alert("Something Went Wrong");
        }
    });
}


function confirmDelete(message, handler) {
    $(`<div class="modal fade" id="confirmModal" role="dialog"> 
     <div class="modal-dialog"> 
       <!-- Modal content--> 
        <div class="modal-content"> 
           <div class="modal-body" style="padding:30px;"> 
             <h4 class="text-center">${message}</h4> 
             <div class="text-center"> 
               <a class="btn btn-danger btn-yes">yes</a> 
               <a class="btn btn-default btn-no">no</a> 
             </div> 
           </div> 
       </div> 
    </div> 
  </div>`).appendTo('body');

    //Trigger the modal
    $("#confirmModal").modal({
        backdrop: 'static',
        keyboard: false
    });

    //Pass true to a callback function
    $(".btn-yes").click(function () {
        handler(true);
        $("#confirmModal").modal("hide");
    });

    //Pass false to callback function
    $(".btn-no").click(function () {
        handler(false);
        $("#confirmModal").modal("hide");
    });

    //Remove the modal once it is closed.
    $("#confirmModal").on('hidden.bs.modal', function () {
        $("#confirmModal").remove();
    });
}



function confirmEdit(message, handler) {
    $(`<div class="modal fade" id="editModal" role="dialog"> 
     <div class="modal-dialog"> 
       <!-- Modal content--> 
        <div class="modal-content"> 
           <div class="modal-body" style="padding:30px;"> 
             <h4 class="text-center">${message}</h4> 
                            <label for="ID" style="margin-top: 20px;"><b>ID</b></label>
                            <input type="text" name="ID" id="editID" disabled="true" placeholder="ID" required="">
                            <label for="Name" style="margin-top: 20px;"><b>Name</b></label>
                            <input type="text" name="name" id="editName" placeholder="Name" required="">
                            <label for="ShortName"><b>ShortName</b></label>
                            <input type="text" name="shortName" id="editShortName" placeholder="ShortName" required="">
                            <label for="Reknown"><b>Reknown</b></label>
                            <input type="text" name="reknown" id="editReknown" placeholder="Reknown" required="">
                            <label for="Bio"><b>Bio</b></label>
                            <textarea draggable="auto" placeholder="Bio" id="editBio" cols="50" rows="5"></textarea>
                            <br/>
             <div class="text-center"> 
                <a class="btn btn-default btn-no">Cancel</a> 
               <a class="btn btn-danger btn-yes">Update</a> 
             </div> 
           </div> 
       </div> 
    </div> 
  </div>`).appendTo('body');

    //Trigger the modal
    $("#editModal").modal({
        backdrop: 'static',
        keyboard: false
    });

    //Pass true to a callback function
    $(".btn-yes").click(function () {
        handler(true);
        $("#editModal").modal("hide");
    });

    //Pass false to callback function
    $(".btn-no").click(function () {
        handler(false);
        $("#editModal").modal("hide");
    });

    //Remove the modal once it is closed.
    $("#editModal").on('hidden.bs.modal', function () {
        $("#editModal").remove();
    });
}

