﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Sql;
using System.Web.Mvc;
using MySql.Data.MySqlClient;

namespace TESTING.Models
{
    public class Talents
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Reknown { get; set; }
        public string Bio { get; set; }
        public string LocalBase64 { get; set; }


        public Talents()
        {

        }


        public Talents(int talentId, string name, string shortName, string reknown, string bio, string localbase64)
        {
            Id = talentId;
            Name = name;
            ShortName = shortName;
            Reknown = reknown;
            Bio = bio;
            LocalBase64 = localbase64;
        }
    }
}