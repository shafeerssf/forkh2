﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TESTING.Models
{
    public class TalentRepository : ITalentRepository
    {
        private List<Talents> talents = new List<Talents>();
        private int _nextId = 1;
        string connString = string.Format("Server=localhost; database=users; UID=root; password=12345;  SslMode = none");


        public IEnumerable<Talents> GetAll()
        {

                MySqlConnection connection = new MySqlConnection(connString);
                MySqlCommand cmd;
                DataTable dt;
                //string insertQuery = "INSERT into user(Name,Email,Password) values ('" + obj.Name + "','" + obj.Email + "','" + obj.Password +"'); ";
                string getQuery = "SELECT * FROM csc";

                connection.Open();
                cmd = new MySqlCommand(getQuery, connection);
                cmd.ExecuteNonQuery();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
            if (!talents.Any())
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Talents talent = new Talents();
                    talent.Id = Convert.ToInt32(dt.Rows[i]["id"]);
                    talent.Name = dt.Rows[i]["Name"].ToString();
                    talent.ShortName = dt.Rows[i]["ShortName"].ToString();
                    talent.Reknown = dt.Rows[i]["Reknown"].ToString();
                    talent.Bio = dt.Rows[i]["Bio"].ToString();
                    talents.Add(talent);
                }
            }
                
                connection.Close();
            
            return talents;
        }

        public Talents Get(int id)
        {
            return talents.Find(t => t.Id == id);
        }

        public Talents Add(Talents item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            item.Id = _nextId++;
            talents.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            MySqlConnection connection = new MySqlConnection(connString);
            MySqlCommand cmd;

            string deleteQuery = "DELETE FROM csc where id=" + id;
            connection.Open();
            cmd = new MySqlCommand(deleteQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            talents.RemoveAll(p => p.Id == id);
        }

        public bool Update(Talents item)
        {
            MySqlConnection connection = new MySqlConnection(connString);
            MySqlCommand cmd;

            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = talents.FindIndex(t => t.Id == item.Id);

            string updateQuery = "UPDATE csc SET Name = @Name, ShortName = @ShortName, Reknown = @Reknown, Bio = @Bio where id=" + item.Id;
           connection.Open();
            cmd = new MySqlCommand(updateQuery, connection);
            cmd.Parameters.AddWithValue("@Name", item.Name);
            cmd.Parameters.AddWithValue("@ShortName", item.ShortName);
            cmd.Parameters.AddWithValue("@Reknown", item.Reknown);
            cmd.Parameters.AddWithValue("@Bio", item.Bio);

            cmd.ExecuteNonQuery();

            connection.Close();

            talents.RemoveAt(index);
            talents.Add(item);
            return true;
        }
    }
}