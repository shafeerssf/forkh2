﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using TESTING.Models;

namespace TESTING.Controllers
{
    public class TalentController : ApiController
    {
        string accessKey = WebConfigurationManager.AppSettings["AWSAccessKey"].ToString();
        string secretKey = WebConfigurationManager.AppSettings["AWSSecretKey"].ToString();
        string sessionToken = WebConfigurationManager.AppSettings["AWSSessionToken"].ToString();
        static readonly ITalentRepository repository = new TalentRepository();

        // Get the objects in S3 bucket
        public ListObjectsResponse Get()
        {
            string bucketName = "talentimages";
            AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, sessionToken, RegionEndpoint.APSoutheast1);
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;
            ListObjectsResponse response = client.ListObjects(request);
            return response;
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/talent")]
        public IEnumerable<Talents> GetAllTalents()
        {
            return repository.GetAll();
        }

        [Route("api/talent/{id:int}")]
        public Talents GetTalent(int id)
        {
            Talents item = repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        [Route("api/talent/{id:int}")]
        public void DeleteProduct(int id)
        {
            Talents item = repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove(id);
        }

        [Route("api/talent")]
        public void PutProduct(Talents talent)
        {
            Talents updateTalent = new Talents();
            updateTalent.Id = talent.Id;
            updateTalent.Name = talent.Name;
            updateTalent.ShortName = talent.ShortName;
            updateTalent.Reknown = talent.Reknown;
            updateTalent.Bio = talent.Bio;
            if (!repository.Update(updateTalent))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
