﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using TESTING.Models;

namespace TESTING.Controllers
{
    public class ValuesController : ApiController
    {
        string accessKey = WebConfigurationManager.AppSettings["AWSAccessKey"].ToString();
        string secretKey = WebConfigurationManager.AppSettings["AWSSecretKey"].ToString();
        string sessionToken = WebConfigurationManager.AppSettings["AWSSessionToken"].ToString();
        static readonly ITalentRepository repository = new TalentRepository();

        public ListObjectsResponse Get()
        {
            string bucketName = "talentimages";
            AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, sessionToken, RegionEndpoint.APSoutheast1);
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;
            ListObjectsResponse response = client.ListObjects(request);
            return response;
        }

        // PUT api/values/5
        /* public void Put(int id, [FromBody]string value)
         {

         }

         // DELETE api/values/5
         public void Delete(int id)
         {
         }

         public void changeToPublic(string filename)
         {
             AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, sessionToken, RegionEndpoint.APSoutheast1);

             PutACLRequest request = new PutACLRequest();
             request.BucketName = "talentimages";
             request.CannedACL = S3CannedACL.PublicRead;
             client.PutACL(request);
         }

         public void changeToPrivate(string filename)
         {
             AmazonS3Client client = new AmazonS3Client(accessKey, secretKey, sessionToken, RegionEndpoint.APSoutheast1);

             PutACLRequest request = new PutACLRequest();
             request.BucketName = "talentimages";
             request.CannedACL = S3CannedACL.Private;
             PutPublicAccessBlockRequest publicAccessBlock = new PutPublicAccessBlockRequest();
             publicAccessBlock.PublicAccessBlockConfiguration.BlockPublicAcls = true;
             publicAccessBlock.BucketName = "talentimages";
             publicAccessBlock.PublicAccessBlockConfiguration.BlockPublicPolicy = true;
             publicAccessBlock.PublicAccessBlockConfiguration.IgnorePublicAcls = true;
             publicAccessBlock.PublicAccessBlockConfiguration.RestrictPublicBuckets = true;
             client.PutACL(request);
             client.PutPublicAccessBlock(publicAccessBlock);


         }*/
/*
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/values")]
        public IEnumerable<Talents> GetAllTalents()
        {
            return repository.GetAll();
        }

        [Route("api/values/{id:int}")]
        public Talents GetTalent(int id)
        {
            Talents item = repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        [Route("api/values/{id:int}")]
        public void DeleteProduct(int id)
        {
            Talents item = repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove(id);
        }


        public void PutProduct(int id, Talents talent)
        {
            talent.Id = id;
            if (!repository.Update(talent))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }*/



    }
}